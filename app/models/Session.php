<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table='sessions';

    public function commission_dossier()
    {
        return $this->belongsToMany(commission_dossier::class,'id_session');
    }
    public  function dossier()
{
    return $this->hasMany(Dossier::class,'id_session');
}
}
