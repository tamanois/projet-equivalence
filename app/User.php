<?php

namespace App;

use App\models\commission_dossier;
use App\models\fonction;
use App\models\Role_utilisateur;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function commision_dossier()
    {
        return $this->belongsToMany(commission_dossier::class,'id_user');
    }
    public  function fonction()
    {
        return $this->belongsTo(fonction::class,'id_fonction');
    }
    public function role_utilisateur()
    {
        return $this->belongsToMany(Role_utilisateur::class,'id_user');
    }

}
