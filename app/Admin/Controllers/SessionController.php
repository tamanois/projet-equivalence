<?php

namespace App\Admin\Controllers;

use App\Models\Session;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SessionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Liste des Sessions d\'équivalences';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Session());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->like('nom', 'nom de la session d\'équivalence');
        });
        $grid->column('id', __('Identifiants'));
     //   $grid->column('numero', __('Numero'));
        $grid->column('nom', __('Nom'));
        $grid->column('annee', __('Année'));
        $grid->column('created_at', __('Date de création'));
        $grid->column('updated_at', __('Date de création'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Session::findOrFail($id));

        $show->field('id', __('Id'));
       // $show->field('numero', __('Numero'));
        $show->field('nom', __('Nom'));
        $show->field('annee', __('Annee'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Session());

       // $form->number('numero', __('Numero'));
        $form->text('nom', __('Nom'));
        $form->text('annee', __('Année'));

        return $form;
    }
}
