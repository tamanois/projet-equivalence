<?php

namespace App\Admin\Controllers;

use App\Models\pays;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class paysController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'pays';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new pays());

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
        
            // Add a column filter
            $filter->like('nom', 'nom');
        
        });
        $grid->column('id', __('Id'));
        $grid->column('nom_fr', __('Nom fr'));
        $grid->column('nom_en', __('Nom en'));
        $grid->column('created_at', __('Created at'))->hide();
        $grid->column('updated_at', __('Updated at'))->hide();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(pays::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom_fr', __('Nom fr'));
        $show->field('nom_en', __('Nom en'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new pays());

        $form->text('nom_fr', __('Nom fr'));
        $form->text('nom_en', __('Nom en'));

        return $form;
    }
}
