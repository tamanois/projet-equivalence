<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Candidat_utilisateur extends Model
{
    protected $table = 'candidat_utilisateurs';

    public function dossier()
    {
        return $this->hasMany(Dossier::class,'id_candidat_utilisateur');
    }
}
