<?php

namespace App\Admin\Controllers;

use App\models\Candidat_utilisateur;
use App\models\Categorie_equivalence;
use App\models\diplome;
use App\models\Dossier;
use App\models\equivalence_diplome_pays;
use App\models\pays;
use App\models\Universite;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;


class DossierController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Liste des dossiers des candidats';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Dossier());
        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->like('date_depot','entrée la date de dépot du dossier');
        });
       /* $grid->column('id_pays', __('Pays'))->display(function ($title,$column){
            $p=pays::find($this->id_pays);
            return $p->nom_fr;*/
        $grid->column('id', __('Identifiant(s)'));
        $grid->column('candidat_utilisateur', __('Nom(s) du candidat(s)'))->display(function ($title,$column)
        {
            $c=Candidat_utilisateur::find($this->candidat_utilisateur);
            return $c->nom.' '.$c->prenom;
        });
        $grid->column('id_pays', __('Pays'))->display(function ($title,$column)
        {
            $p=pays::find($this->id_pays);
            return $p->nom_fr.' '.$p->nom_en;
        });
        $grid->column('id_categorie_equivalence', __('Id categorie equivalence'))->display(function ($title,$column)
        {
            $ca=Categorie_equivalence::find($this->id_categorie_equivalence);
            return $ca->nom_fr;
        });
        $grid->column('id_universite', __('Université(s)'));
       // $grid->column('id_session', __('Id session'));
      //  $grid->column('id_etat', __('Id etat'));
        $grid->column('diplome_a_authentifie', __('Diplome d\'équivalence à délivrer'));
        $grid->column('id_diplome', __('Diplome'))->display(function ($title,$column)
        {
            $d=diplome::find($this->id_diplome);
            return $d->nom;
        });
        $grid->column('type_diplome', __('Catégorie du diplome'));
        $grid->column('diplome', __('Liste des Diplomes d\'equivalences'));
        $grid->column('acte_naissance', __('Acte de  naissance'));
        $grid->column('cni', __('Carte d\'identité'));
        $grid->column('annee_obtention_diplome', __('Année d\'obtention diplome'));
        $grid->column('date_delivrance', __('Date de  délivrance'));
        $grid->column('programme_etude', __('Programme de formation'));
        $grid->column('date_depot', __('Date de  dépot'));
        $grid->column('cv', __('Curriculum vitae'));
        $grid->column('duree_diplome', __('Durée du diplome'));
        $grid->column('created_at', __('Date de création'));
       // $grid->column('updated_at', __('Date de mise à jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Dossier::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('candidat_utilisateur', __('Candidat utilisateur'));
        $show->field('id_pays', __('Id pays'));
        $show->field('id_categorie_equivalence', __('Id categorie equivalence'));
        $show->field('id_universite', __('Id universite'));
        $show->field('id_session', __('Id session'));
        $show->field('id_etat', __('Id etat'));
        $show->field('diplome_a_authentifie', __('Diplome a authentifie'));
        $show->field('id_diplome', __('Id diplome'));
        $show->field('type_diplome', __('Type diplome'));
        $show->field('diplome', __('Diplome'));
        $show->field('acte_naissance', __('Acte naissance'));
        $show->field('cni', __('Cni'));
        $show->field('annee_obtention_diplome', __('Annee obtention diplome'));
        $show->field('date_delivrance', __('Date delivrance'));
        $show->field('programme_etude', __('Programme etude'));
        $show->field('date_depot', __('Date depot'));
        $show->field('cv', __('Curriculum vitae'));
        $show->field('duree_diplome', __('Duree diplome'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        \Encore\Admin\Facades\Admin::js('test.js');
        $form = new Form(new Dossier());
        $pays=['0'=>'Selectionnez le pays'];
        $p=pays::all();
        foreach ($p as $pay)
        {
            $pays[$pay->id]=$pay->nom_fr;
        }
        $diplome=['0'=>'Selectionnez le Diplome'];
        $d=diplome::all();
        foreach ($d as $dip)
        {
            $diplome[$dip->id]=$dip->nom;
        }
        $universite=['0'=>'Sélectionnez l\'université correspondante'];
        $u=Universite::all();
        foreach ($u as $item) {
            $universite[$item->id]=$item->nom_fr;
        }
        $candidat=['0'=>'Selectionnez le candidat'];
        $c=Candidat_utilisateur::all();
        foreach ($c as $item)
        {
            $candidat[$item->id]=$item->nom.' '.$item->prenom;
        }
        $categorie=['0'=>'Selectionnez la categorie de l\'equivalence'];
        $cas=Categorie_equivalence::all();
        foreach ($cas as $ct)
        {
            $categorie[$ct->id]=$ct->nom_fr.' / '.$ct->nom_en;
        }
     //   $d=Admin::user()->id;





       // $form->hidden('user_id')->value($d);
        $form->select('candidat_utilisateur', __('Nom du candidat'))->options($candidat);
        $form->select('id_pays', __('Pays'))->options($pays);
        $form->select('id_diplome', __('Sélectionnez le diplome à etablir l\'équivalence'))->options($diplome);
        $form->select('id_categorie_equivalence', __('Id categorie equivalence'))->options($categorie);
        $form->select('id_universite', __('Université'))->options($universite);
        $form->text('type_diplome', __(' Catégorie de diplome '))->placeholder('EX: licence professionnelle en gestion des systemes d\'informations');
        $form->text('duree_diplome', __('Durée diplome'));
        $form->date('date_delivrance', __('Date delivrance'))->default(date('Y-m-d'));
        $form->text('annee_obtention_diplome', __('Année obtention du diplome'));
        $form->date('date_depot', __('Date  de dépot'))->default(date('Y-m-d'));
        $form->file('diplome_a_authentifie', __('Diplome à authentifié'));

        $equivalences = equivalence_diplome_pays::all();
        foreach ($equivalences as $e)
        {
            $form->file('diplome', __('Diplome '))->value('')->setElementClass(''.$e->id_pays.''.$e->id_diplome.' hide');
        }





        $form->file('acte_naissance', __('Acte de  naissance'));
        $form->file('cni', __('Carte d\'identité'));
        $form->file('cv', __('Curriculum vitae'));
        $form->file('programme_etude', __('Programme de formation'));

        /*$form->submitted(function (Form $form)
        {
            dump($form);
            die();
        });*/
        // $form->number('id_session', __('Id session'));
       // $form->number('id_etat', __('Id etat'));

        return $form;
    }
}
