<?php

namespace App\Admin\Controllers;

use App\Models\Candidat_utilisateur;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class Candidat_utilisateurController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Liste des candidats';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Candidat_utilisateur());

        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->like('nom','nom du candidat recherché');
        });
        $grid->column('id', __('Identifiants'));
        $grid->column('nom', __('Noms'));
        $grid->column('prenom', __('Prenoms'));
        $grid->column('date_nais', __('Date de naissance'));
        $grid->column('lieu_nais', __('Lieu de naissance'));
        $grid->column('telephone', __('Téléphone(s)'));
        $grid->column('profession', __('Proféssions'));
        $grid->column('email', __('Email'));
        $grid->column('created_at', __('Date de création'));
        $grid->column('updated_at', __('Date de la mise à jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Candidat_utilisateur::findOrFail($id));

        $show->field('id', __('Identifiant(s)'));
        $show->field('nom', __('Noms'));
        $show->field('prenom', __('Prenoms'));
        $show->field('date_nais', __('Date de  naissance'));
        $show->field('lieu_nais', __('Lieu  de naissance'));
        $show->field('telephone', __('Téléphone'));
        $show->field('profession', __('Proféssion'));
        $show->field('email', __('Email'));
        $show->field('created_at', __('Date de création'));
        $show->field('updated_at', __('Date de  la mise à jour'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Candidat_utilisateur());

        $form->text('nom', __('Nom(s)'));
        $form->text('prenom', __('Prénom(s)'));
        $form->date('date_nais', __('Date de naissance'))->default(date('Y-m-d'));
        $form->text('lieu_nais', __('Lieu de naissance'));
        $form->text('telephone', __('Téléphone(s)'));
        $form->text('profession', __('Proféssion'));
        $form->email('email', __('Email'));

        return $form;
    }
}
