<?php

namespace App\models;

use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\Eloquent\Model;

class fonction extends Model
{
    public  function user()
    {
        return $this->belongsToMany(Role::class,'id_fonction');
    }
    public function admin_user()
    {
        return $this->hasMany(admin_user::class,'id_fonction');
    }
    public function coter_dossier_expediteur()
    {
        return $this->hasMany(coter_dossier::class,'id_fonction_expediteur');
    }
    public function coter_dossiers_destinateur()
    {
        return $this->hasMany(coter_dossier::class,'id_fonction_destinateur');
    }
}
