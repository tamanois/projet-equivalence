<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquivalenceDiplomePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equivalence_diplome_pays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pays')->default(0);
            $table->integer('id_diplome')->default(0);
            $table->string('liste_diplome');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equivalence_diplome_pays');
    }
}
