<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfilController extends Controller
{
    //
    protected $values=[];
    public function __construct()
    {
       
        $this->values['big_title']='Mon profil';

        $this->values['title']='profil';
        
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        //
        $user = $request->user();

        $this->values['user']=$user;
       
        return view('profile', $this->values);

    }
}
