<?php

namespace App\Http\Controllers;

use App\models\Dossier;
use App\models\diplome;
use App\models\equivalence_diplome_pays;
use App\models\pays;
use App\models\Candidat_utilisateur;
use App\models\Universite; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DossierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $values=[];
    public function __construct(Request $request)
    {
        $user = $request->user();
        //
        
        $dossiersp = equivalence_diplome_pays::all();
        $diplomes = diplome::all();
        $pays = pays::all();
        
        $universites = Universite::all();
        $this->values['big_title']='MES DOSSIERS';

        $this->values['title']='dossier';
        $this->values['user']= $user;
        $this->values['dossiers_p']= $dossiersp;
        $this->values['diplomes']= $diplomes;
        $this->values['pays']= $pays;
        $this->values['universites']= $universites;
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        //
        $user = $request->user();
        
        $this->values['user']=$user;
        $utilisateur = Candidat_utilisateur::where('email', '=',$user->email)->get()[0];
        
        $dossiers = Dossier::where('candidat_utilisateur', '=', $utilisateur->id)->get();
        $this->values['dossiers']= $dossiers;
        /* dump($dossiers);
        die(); */
        return view('dossier', $this->values);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = $request->user();
        $utilisateur = Candidat_utilisateur::where('email', '=',$user->email)->get()[0];

        $data = $request->all();
        
        unset($data['_token']);

        

        if($request->file()) {
            /* $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');

            $fileModel->name = time().'_'.$req->file->getClientOriginalName();
            $fileModel->file_path = '/storage/' . $filePath;
            $fileModel->save(); */

            $data['diplome_a_authentifie'] = $data['diplome_a_authentifie']->storeAs(
                ''.$utilisateur->id, time().'_'.$data['cni']->getClientOriginalName()
            );
            $data['diplome_a_authentifie'] = time().'_'.$data['cni']->getClientOriginalName();

            $data['cni'] = $data['cni']->storeAs(
                ''.$utilisateur->id, time().'_'.$data['cni']->getClientOriginalName()
                );
            $data['cni'] = time().'_'.$data['cni']->getClientOriginalName();
             
            $data['acte_naissance'] = $data['acte_naissance']->storeAs(
                    ''.$utilisateur->id, time().'_'.$data['acte_naissance']->getClientOriginalName()
                );
            $data['acte_naissance'] = time().'_'.$data['acte_naissance']->getClientOriginalName();

            $data['programme_etude'] = $data['programme_etude']->storeAs(
                    ''.$utilisateur->id, time().'_'.$data['programme_etude']->getClientOriginalName()
                );
            $data['programme_etude'] =  time().'_'.$data['programme_etude']->getClientOriginalName();

            $data['cv'] = $data['cv']->storeAs(
                    ''.$utilisateur->id, time().'_'.$data['cv']->getClientOriginalName()

            );
            $data['cv'] = time().'_'.$data['cv']->getClientOriginalName();

            if (isset($data['diplome']))
            {
                foreach ($data['diplome'] as $key => $value)
                {
                    //store file
                    $data['diplome'][$key] = $data['diplome'][$key]->storeAs(
                        ''.$utilisateur->id, 'diplome_additif'.'_'.$data['diplome'][$key]->getClientOriginalName()
                    );
                    //save filename
                    $data['diplome'][$key] = 'diplome_additif'.'_'.$data['diplome'][$key]->getClientOriginalName();
                }
            } 
        }
        

        
        /* $image = null;
        if (request()->hasFile($data['fichier_equivalence'])) {
            $file = request()->file('image');
            $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./uploads/', $image);
        } */
        $data['candidat_utilisateur'] = $utilisateur->id;
        /* dump($data);
        die(); */
        /*$dossier = new Dossier();
        $dossier->candidat_utilisateur = $utilisateur->id;
        $dossier->id_diplome = $data['diplome_pour_equivalence'];
        $dossier->diplome_a_authentifie = $data['fichier_equivalence'];
        $dossier->id_pays = $data['pays'];
        $dossier->id_universite = $data['universite'];
        $dossier->diplome = $data['diplome'];
        $dossier->id_universite = $data['universite'];*/
        /* dump($data);
        die(); */
        Dossier::create($data);
        return redirect('/dossiers')->withSuccess(['ok'=>'']); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Dossier  $dossier
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        //
        $dossier = Dossier::find($id);
        dump($dossier);
    }

    public function download($file, Request $request)
    {
        $user = $request->user();;
        $utilisateur = Candidat_utilisateur::where('email', '=',$user->email)->get()[0];

        $pathToFile = storage_path('app\\'.$utilisateur->id.'\\'.$file);
        /* dump($pathToFile);
        die(); */
        return response()->download($pathToFile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Dossier  $dossier
     * @return \Illuminate\Http\Response
     */
    public function edit(Dossier $dossier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Dossier  $dossier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dossier $dossier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Dossier  $dossier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dossier $dossier)
    {
        //
    }
}
