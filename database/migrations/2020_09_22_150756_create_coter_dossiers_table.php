<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoterDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coter_dossiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dossier')->default(0);
            $table->integer('id_admin_user_destinaire')->default(0);
            $table->integer('id_admin_user_expediteur')->default(0);
            $table->integer('id_fonction_expediteur')->default(0);
            $table->integer('id_fonction_destinateur')->default(0);
            $table->integer('id_etat')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coter_dossiers');
    }
}
