<?php

namespace App\Admin\Controllers;

use App\Models\Role_utilisateur;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class Role_utilisateurController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Role_utilisateur';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Role_utilisateur());

        $grid->column('id', __('Id'));
        $grid->column('id_role', __('Id role'));
        $grid->column('id_user', __('Id user'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Role_utilisateur::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('id_role', __('Id role'));
        $show->field('id_user', __('Id user'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Role_utilisateur());

        $form->number('id_role', __('Id role'));
        $form->number('id_user', __('Id user'));

        return $form;
    }
}
