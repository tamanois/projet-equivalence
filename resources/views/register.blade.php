@extends('baselogin')

@section('content')

<body class="hold-transition row " style="background: url(img/balance.svg) no-repeat center; width:100%;">
<div class="login-box col-md-6 offset-md-3 " style="opacity:0.97; " >
  <div class="login-logo" style="margin-bottom:0px; margin-top:50px; box-shadow:1px 1px 5px">
    <div style="height:140px; background:#5B7693; margin:auto;">
        <a href="index2.html" class="" >
          <img src="img/logominesup.jpg" style="height:130px; margin-left:0%; padding:05px;" alt=" Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
<!--          <span class="brand-text font-weight-light"></span>-->
        </a>
    </div>
  </div>
  <!-- /.login-logo -->
  <div class="card " style="border-radius:0px; box-shadow:1px 1px 5px">
    <div class="card-body login-card-body ">
      <p class="login-box-msg">Sign up to start your session</p>

      <form method="POST" class='row' action="{{ route('register') }}">
                        @csrf
        <div class="input-group mb-3 col-md-6">
          <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope" style="color:#5B7693;"></span>
            </div>
          </div>
           @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
           @enderror
        </div>
        <div class="input-group mb-3 col-md-6">
          <input id="name" placeholder='Nom' type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-hashtag" style="color:#5B7693;"></span>
            </div>
          </div>
          @error('name')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3 col-md-6">
          <input type="text" class="form-control" name="prenom" placeholder="Prenom">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-hashtag" style="color:#5B7693;"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3 col-md-6">
          <input type="text" class="form-control" name="lieu_naissance" placeholder="Lieu de naissance">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-map-marker-alt" style="color:#5B7693;"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3 col-md-6">
          <input type="date" class="form-control" name="date_naissance" placeholder="Date de naissance" style="color:#5B7693;">

        </div>
        <div class="input-group mb-3 col-md-6">
          <input id="password" type="password" placeholder='Mot de passe' class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock" style="color:#5B7693;"></span>
            </div>
          </div>
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3 col-md-6">
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock" style="color:#5B7693;"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3 col-md-6">
          <input type="tel" class="form-control" name="telephone" placeholder="Telephone">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone" style="color:#5B7693;"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3 col-md-12">
          <input type="tel" class="form-control" name="profession" placeholder="Profession">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-tie" style="color:#5B7693;"></span>
            </div>
          </div>
        </div>
        <div class="" style="margin-top: 20px; margin:auto;">
          <!-- /.col -->
          <div class="col-md-12 m-auto">
            <button type="submit" class="btn  btn-block" style="background:#C73F01; color:white; "><i class="fas fa-user-plus"></i> Sign up
               </button>
            <p class="mb-2" style="margin-top: 20px;">
              <a href="{{ route('login1') }}" style="color:#5B7693;"> <i class="fas fa-sign-in-alt"></i>  Go to login page</a>
            </p>
          </div>
          <!-- /.col -->
        </div>
      </form>



    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

@endsection
