<?php

namespace App\models;

use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\Eloquent\Model;
use Tests\Models\User;

class Role_utilisateur extends Model
{
    protected $table='role_utilisateurs';
    public function user()
    {
        return $this->belongsToMany(User::class,'id_user');
    }
    public function role()
    {
        return $this->belongsToMany(Role::class,'id_role');
    }
}
