<?php

namespace App\Admin\Controllers;

use App\Models\Parametre;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ParametreController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Parametre';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Parametre());

        $grid->column('id', __('Id'));
        $grid->column('nom_directeur', __('Nom directeur'));
        $grid->column('logo', __('Logo'));
        $grid->column('site', __('Site'));
        $grid->column('email', __('Email'));
        $grid->column('telephone', __('Telephone'));
        $grid->column('nom_direction', __('Nom direction'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Parametre::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom_directeur', __('Nom directeur'));
        $show->field('logo', __('Logo'));
        $show->field('site', __('Site'));
        $show->field('email', __('Email'));
        $show->field('telephone', __('Telephone'));
        $show->field('nom_direction', __('Nom direction'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Parametre());

        $form->text('nom_directeur', __('Nom directeur'));
        $form->text('logo', __('Logo'));
        $form->text('site', __('Site'));
        $form->email('email', __('Email'));
        $form->text('telephone', __('Telephone'));
        $form->text('nom_direction', __('Nom direction'));

        return $form;
    }
}
