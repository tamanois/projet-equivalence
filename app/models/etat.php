<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class etat extends Model
{
    protected $table='etats';
    public function dossier()
    {
        return $this->hasMany(Dossier::class,'id_etat');

    }
    public function etat_dossier()
    {
        return $this->belongsToMany(etat_dossier::class,'id_etat');
    }
    public function coter_dossier()
    {
        return $this->belongsToMany(coter_dossier::class,'id_etat');
    }
}
