<?php

namespace App\Admin\Controllers;

use App\models\diplome;
use App\Models\equivalence_diplome_pays;
use App\models\pays;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class equivalence_diplome_paysController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'equivalence_diplome_pays';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new equivalence_diplome_pays());

        $grid->column('id', __('Id'));
        $grid->column('id_pays', __('Pays'));
        $grid->column('id_diplome', __('Diplome'));
        $grid->column('liste_diplome', __('Liste diplome'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(equivalence_diplome_pays::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('id_pays', __('Pays'));
        $show->field('id_diplome', __('Diplome'));
        $show->field('liste_diplome', __('Liste diplome'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new equivalence_diplome_pays());
        $pays=['0'=>'Selectionnez le pays'];
        $p=pays::all();
        foreach ($p as $pay)
        {
            $pays[$pay->id]=$pay->nom_fr;
        }
        $diplo=['0'=>'Selectionnez le diplome'];
        $d=diplome::all();
        foreach ($d as $dip)
        {
            $diplo[$dip->id]=$dip->nom;
        }


        $form->select('id_pays', __('Id pays'))->options($pays);
        $form->select('id_diplome', __('Id diplome'))->options($diplo);
        $form->textarea('liste_diplome', __('Liste diplome'));

        return $form;
    }
}
