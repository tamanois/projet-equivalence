<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Categorie_equivalence extends Model
{
    protected $table='Categorie_equivalences';

    public function dossier()
    {
        return $this->hasMany(Dossier::class,'id_categorie_equivalence');

    }
}
