<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $values=[];
    public function login()
    {
        return view('login');
    }
    public function logout()
    {
        
        Auth::logout();
        return view('login');
    }
    public function register()
    {
        return view('register');
    }
    public function home()
    {
        return view('dossier');
    }
    
}
