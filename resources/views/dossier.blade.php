@extends('base')

@section('main')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2" style="margin-top:20px;">
              <div class="col-sm-6">
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item " ><a href="index2.html">Accueil</a></li>
                  <li class="breadcrumb-item active">Mes dossiers</li>

<!--
                  <li class="breadcrumb-item active">Simple Tables</li>
-->
                </ol>
              </div>
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <div class="row">
          <div class="card col-md-12">
              <div class="card-header">
                <h3 class="card-title text-uppercase" style="color:#5B7693; font-size:1.2em; font-weight:bold;" >Mes dossiers </h3>

                <div class="card-tools">
                  <!--<ul class="pagination pagination-sm float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                  </ul>-->
                  <button class="btn " style="background:#17a2b8; color:white;" data-toggle="modal" data-target="#exampleModal"> <i class="fas fa-plus"></i> Ajouter un nouveau dossier</button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-responsive-sm table-responsive-xs ">
                  <thead>
                    <tr style="color: #474359; text-transform: uppercase; ">
                      <th style="width: 10px">#</th>
                      <th style="">Nom diplôme</th>
                      <th>diplôme à authentifier </th>
                      <th>Documents fournis </th>
                      <th>Actions</th>

                    </tr>
                  </thead>
                  <tbody>
                  
                    @foreach($dossiers as $d)
                          
                        <tr>
                          <td>{{$d->id}}</td>
                          <td style="color:#5B7693; text-transform: uppercase; font-weight:bold;">{{$user->name}} </td>
                          <td>
                              @foreach($diplomes as $dp)
                                
                                  @if($d->id_diplome == $dp->id)
                                      {{$dp->nom}}
                                  @endif
                              @endforeach
                              
                             
                          <td>
                            <ul>
                              {{-- <li>cv : {{$d->cv}} </li>
                              <li>acte de naissance : {{$d->acte_naissance}} </li>
                              <li>CNI : {{$d->cni}} </li>
                              <li>Progromme de formation  : {{$d->programme_etude}} </li> --}}
                              @if ($d->diplome)

                                <ul>
                                  
                           
                                  @foreach($d->diplome as $key => $value ) 
                                    <li><a href="{{ route('download', $value) }}" target="_blank">
                                     {{$key}} </a></li>
                                  @endforeach
                                </ul>
                              @endif
                            </ul>
                          </td>
                          
                          <td>
                            <a href="{{{URL::to('/dossiers/').'/'.$d->id }}}}" class="btn " style="background:#D20762; color:white; "> <i class="fas fa-eye"></i>  </a>
                            <button class="btn" style="background:#5B5C74; color:white;"> <i class="fas fa-edit"></i>  </button>
                            <button class="btn" style="background:#C73F01; color:white;"> <i class="fas fa-trash"></i>  </button>
{{--                             <button class="btn" style="background:#4E69A4; color:white;"> <i class="fas fa-envelope"></i>   </button>
 --}}                          </td>
                        </tr>
                    @endforeach
                    

                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>

        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->
<div class="modal fade col-md-12" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header" >
        <h5 class="modal-title" id="exampleModalLabel" ></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card ">
              <div class="card-header text-center " style="background:#17a2b8; color:#fff; text-align:center;">
                <h3 class="card-title text-center  " style="width:100%;"> <i class="fas fa-folder-plus"></i>   Enregistrer un nouveau dossier</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" class="row " style="padding:20px;" action="{{ url('/dossiers') }}" accept-charset="UTF-8" enctype="multipart/form-data" method="post" >
                        @csrf
                <div class="card-body col-md-12 row" style="margin:0px; padding:0px;">
                 
                  <div class="form-group col-md-6" >
                    <label>Nom du diplome à authentifier</label>
                    <select id="diplome_pour_equivalence" name="id_diplome" class="form-control" style="width: 100%;">
                      <option  value="0">Choissez votre diplome</option>
                      @foreach($diplomes as $dp)
                        <option id="{{$dp->id}}" value="{{$dp->id}}">{{$dp->nom}}</option>
                      @endforeach
                    </select>
                    
                  </div>
                  <div class="form-group col-md-6">
                    <label for="fichier_equivalence">Importer le diplome</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input accept="application/pdf,application/vnd.ms-word" type="file"  class="custom-file-input" id="fichier_equivalence" name="diplome_a_authentifie">
                        <label class="custom-file-label" for="fichier_equivalence">Choisir le ficher</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id=""><i class="fas fa-file"></i> </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-md-6" >
                    <label>Filère de formation </label>
                    <input type="text" class="form-control" id="type_diplome" name="type_diplome" placeholder="Exemple: Comptabilité">  
                  </div>
                  <div class="form-group col-md-6" >
                    <label>Pays  obtention</label>
                    <select id="pays" name="id_pays" class="form-control select2 " style="width: 100%;" >
                      
                      @foreach($pays as $p)
                        <option id="{{$p->id}}" value="{{$p->id}}">{{$p->nom_fr}}</option>
                      @endforeach
                    </select>
                    
                  </div>
                  <div class="form-group col-md-6" >
                    <label>Université  obtention</label>
                    <select id="universite" class="form-control select2bs42 " style="width: 100%;" name="id_universite"  tabindex="-1" aria-hidden="true">
                      <option  value="0">Choissez votre université</option>
                      @foreach($universites as $u)
                        <option style="display:none;" class="{{$u->id_pays}}" value="{{$u->id}}">{{$u->nom_fr}}</option>
                      @endforeach
                    </select>
                    
                  </div>
                  <div class="form-group col-md-6">
                    <label>Date de délivrance</label>

                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="date" class="form-control" name="date_delivrance" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false">
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class="form-group col-md-6">
                    <label>Date de obtention du diplôme</label>

                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="date" class="form-control" name="annee_obtention_diplome" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false">
                    </div>
                    <!-- /.input group -->
                  </div>
                  
                  <div class="form-group col-md-6">
                    <label for="carte_identité">Carte nationnal identité</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input accept="application/pdf,application/vnd.ms-word" type="file"  class="custom-file-input" id="carte_identité" name="cni">
                        <label class="custom-file-label" for="carte_identité">Choisir le ficher</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id=""><i class="fas fa-file"></i> </span>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group col-md-6">
                    <label for="acte_naissance">Acte de naissance</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input accept="application/pdf,application/vnd.ms-word" type="file"  class="custom-file-input" id="acte_naissance" name="acte_naissance">
                        <label class="custom-file-label" for="acte_naissance">Choisir le ficher</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id=""><i class="fas fa-file"></i> </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="acte_naissance">Progromme de formation suivi</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input accept="application/pdf,application/vnd.ms-word" type="file"  class="custom-file-input" id="programme_formation" name="programme_etude">
                        <label class="custom-file-label" for="programme_formation">Choisir le ficher</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id=""><i class="fas fa-file"></i> </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="acte_naissance">Votre CV</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input accept="application/pdf,application/vnd.ms-word" type="file"  class="custom-file-input" id="cv" name="cv">
                        <label class="custom-file-label" for="cv">importer le cv</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id=""><i class="fas fa-file"></i> </span>
                      </div>
                    </div>
                  </div>
                  <div id="hide" class="row">
                    <h6 class="alert alert-info" style="width:100% ; text-align:center;"> Diplomes à fournir</h6>
                  @foreach($dossiers_p as $d)
          
                     
                        @if ($d->liste_diplome != "")
                            @foreach(explode(',', $d->liste_diplome) as $info) 
                               <div style="display:none;" class="form-group col-md-6 {{$d->id_diplome}}{{$d->id_pays}}" id="{{$info}}">
                                  <label for="{{$d->id}}{{$info}}">{{$info}}</label>
                                  <div class="input-group">
                                    <div class="custom-file">
                                      <input accept="application/pdf,application/vnd.ms-word" type="file"  name="diplome['{{$info}}']" class="custom-file-input" id="" name="{{$info}}">
                                      <label class="custom-file-label" for="{{$d->id}}{{$info}}">Choisir le ficher</label>
                                    </div>
                                    <div class="input-group-append">
                                      <span class="input-group-text" id=""><i class="fas fa-file"></i> </span>
                                    </div>
                                  </div>
                                </div>
                            @endforeach
                        @endif
                        
                  @endforeach
                  
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer col-md-12">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            

          </div>
      </div>
     {{--  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
  
</div>


@endsection

  
