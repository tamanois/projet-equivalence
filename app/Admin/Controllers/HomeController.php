<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('')
            ->description('')
            ->row('<h1   style="text-align:center ; color:#3c8dbc; text-transform: uppercase;">Tableau de bord</h1>');

          
    }
}
