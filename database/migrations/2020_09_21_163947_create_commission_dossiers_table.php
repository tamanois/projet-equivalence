<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_dossiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dossier')->default(0);
            $table->integer('id_commission_equivalence')->default(0);
            $table->integer('id_session')->default(0);
            $table->integer('id_user')->default(0);
            $table->json('menbres');
            $table->string('chef_commission');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_dossiers');
    }
}
