<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class etat_dossier extends Model
{
    public function etat()
    {
        return $this->belongsToMany(etat::class,'id_etat');
    }
    public function dossier()
    {
        return $this->belongsToMany(Dossier::class,'id_dossier');
    }
}
