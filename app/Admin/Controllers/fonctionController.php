<?php

namespace App\Admin\Controllers;

use App\Models\fonction;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class fonctionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Liste des fonctions';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new fonction());
        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->like('nom','nom de la fonction');
        });

        $grid->column('id', __('Id'));
        $grid->column('nom', __('Nom'));
        $grid->column('created_at', __('Date de création'));
        $grid->column('updated_at', __('Date de la mise à jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(fonction::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom', __('Nom'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new fonction());

        $form->text('nom', __('Nom'));

        return $form;
    }
}
