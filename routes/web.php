<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'AuthController@login')->name('login1');
Route::get('/profil1', 'ProfilController@index')->name('profil1');
Route::get('/logout', 'AuthController@logout')->name('logout1');
Route::get('/inscription', 'AuthController@register')->name('register1');
Route::get('/home', 'CandidatController@index')->name('home');
Route::get('/download/{slug}', 'DossierController@download')->name('download');
Route::resource('dossiers', DossierController::class);
