<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class coter_dossier extends Model
{
    public function dossier()
    {
        return $this->belongsToMany(Dossier::class,'id_dossier');
    }
    public function admin_user_expediteur()
    {
        return $this->belongsToMany(admin_user::class,'id_admin_user_expediteur');
    }
    public  function admin_user_destinataire()
    {
        return $this->belongsToMany(admin_user::class,'id_admin_user_expediteur');
    }
     public  function fonction_expediteur()
     {
        return $this->belongsToMany(fonction::class,'id_fonction_expediteur');
     }
     public function fonction_destinateur()
     {
         return $this->belongsToMany(fonction::class,'id_fonction_destinateur');
     }

}
