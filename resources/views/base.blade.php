<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>MINSUP Gestion des demandes équivalences de diplôme</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      
  <style>
  
    .active2
    {
      border-left:5px solid #D20762;
      background: #3DDFEC;
      color: #fff;
      opacity:.9;

    }
    .active2 i
    {
      border-radius:50%;
      color:white;
      background:#D20762; 
      min-width: 40px; 
      padding:7px;
      opacity:1;
      
    }
  </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="height:80px; border-bottom:1px solid #000;" >
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item" style="color: #474359; font-weight:bolder; font-size:1.5em; float:left; margin-left:0px; text-transform: uppercase">

        <h1 >
          {{$big_title}}
        </h1>
      <!--<li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>-->
    </ul>

    <!-- SEARCH FORM -->
   <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>-->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
<!--      <li class="nav-item dropdown">-->
        <!--<a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>-->
        <!--<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            &lt;!&ndash; Message Start &ndash;&gt;
            <div class="media">
              <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            &lt;!&ndash; Message End &ndash;&gt;
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            &lt;!&ndash; Message Start &ndash;&gt;
            <div class="media">
              <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>-->
            <!-- Message End -->
         <!-- </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            &lt;!&ndash; Message Start &ndash;&gt;
            <div class="media">
              <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>-->
            <!-- Message End -->
         <!-- </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>-->
      <!-- Notifications Dropdown Menu -->



      </li>

      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" >
          <i class="far fa-bell" style="font-size:2.2em; color:#5B5C74;"></i>
          <span class="badge  navbar-badge" style="background:#C73F01; padding:5px; color: #fff; font-size:0.7em; border-radius:50%; border:3px solid white;">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-th-large"></i></a>
      </li>-->
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background:#5B5C74;">
    <!-- Brand Logo -->
    <div style="height:140px; background:#3DDFEC; margin:auto;">
        <a href="index3.html" class="" >
          <img src="{{url('img/logominesup.jpg') }}" style="height:130px; margin-left:25%; padding:05px;" alt=" Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
<!--          <span class="brand-text font-weight-light"></span>-->
        </a>
    </div>


    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user panel (optional) -->


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="{{ route('home') }}" @if($title =='accueil') class="nav-link  active2 " @else class="nav-link"  @endif>
                <i class="nav-icon fas fa-home" style=""></i>
                <span class="text-white">
                  ACCUEIL
                </span>
              </a>
            </li>
            <li class="nav-item ">
              <a href="{{ url('/dossiers') }}" @if($title=='dossier') class="nav-link  active2 " @else class="nav-link"  @endif>
                <i class="nav-icon fas fa-folder"></i>
                <p class="text-white">
                  Mes dossiers

                </p>
              </a>
            <!--<ul class="nav nav-treeview" style="display: none;">
                <li class="nav-item">
                  <a href="{{ url('/dossier') }}" class="nav-link active">
                    <i class="far fa-plus nav-icon"></i>
                    <p>Ajouter un dossier</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="./index2.html" class="nav-link">
                    <i class="far fa-edit nav-icon"></i>
                    <p>Modifier un dossier</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="./index3.html" class="nav-link">
                    <i class="far fa-list nav-icon"></i>
                    <p>Consulter</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="./index3.html" class="nav-link">
                    <i class="far fa-trash nav-icon"></i>
                    <p>Supprimer</p>
                  </a>
                </li>
            </ul>-->
          </li>
            <li class="nav-item">
              <a href=" {{ url('/profil1') }}" @if($title=='profil') class="nav-link  active2 " @else class="nav-link"  @endif>
                <i class="nav-icon far fa-user"></i>
                <p class="text-white">
                  Mon compte
                </p>
              </a>
            </li>
        </ul>

      </nav>
      <div class="user-panel mt-3 pb-3 mb-3 d-flex" style="background:#474359; padding-top:20px; margin-top:80px;">
          <div class="image">
            <i  class="img-circle elevation-2 fas fa-user" style="color:#3DDFEC; font-size:1.8em; height: 40px; width:40px; text-align:center; padding-top:5px;" alt="User Image">
          </i>
          </div>
          <div class="info">
            <a href="#" class="d-block">{{$user->name}}</a> 
            <a href="{{url('/logout')}}" class="d-inline-block">               
              <i class="nav-icon fas fa-sign-out-alt" style="color:#D20762; font-size: 1em; height: 40px; width:40px; text-align:center; padding-top:1px;"></i>Se déconnecter
              
            </a>
            
          </div>
      </div>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!--<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Titre</h1>
          </div>&lt;!&ndash; /.col &ndash;&gt;
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div>&lt;!&ndash; /.col &ndash;&gt;
        </div>&lt;!&ndash; /.row &ndash;&gt;
      </div>&lt;!&ndash; /.container-fluid &ndash;&gt;
    </div>-->
    <!-- /.content-header -->
    @yield('main')

    <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  
  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">ksa</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{ asset('dist/js/demo.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
<script src="{{ asset('dist/js/demo.js') }}"></script>
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
  $(function () {
    {{-- /*$("[type='file']").on("change", function () {
      alert('ok');
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings("[type='file']").addClass("selected").html(fileName);
    }); */--}}
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    $('#hide').hide();
    //Initialize Select2 Elements
<<<<<<< HEAD


    $('.select2').select2()
=======
     
    //$("#test").hide();
    /*$("#exampleModal").change(function(){
        //var diplome = $(this).children("option:selected").val();
        alert("You have selected the diplome - " );
    });*/
    var diplome = 0 ;
    var pays = 0;
    var prec = 0;
    var suiv = 0;
    var universite ;
    /*$("#diplome_pour_equivalence").click(function(){
          diplome = $(this).children("option:selected").val()
        //alert("You have selected the diplome - "+ diplome );
    });*/
     
    pays = $('#pays').children("option:selected").val();
    $('.'+pays).show();
    $("#universite").click(function(){
          $('#hide').hide();
          $('.'+prec).hide();
          
          pays = $('#pays').children("option:selected").val()
          $('.'+pays).show();
          diplome = $("#diplome_pour_equivalence").children("option:selected").val()
          suiv = diplome+pays;
          $('#hide').show(1000);
          $('.'+suiv).show(1000);
          prec  = suiv;
          //alert(diplome+pays);
          
        //alert("You have selected the diplome - "+ diplome );
    });
    
    //$('.select2').select2();
>>>>>>> master

    //Initialize Select2 Elements
    /*$('#diplome_pour_equivalence').select2({
      theme: 'bootstrap4',
    });*/
    /*$('#pays').select2({
      theme: 'bootstrap4',
    });
    $('.select2bs42').select2({
      theme: 'bootstrap4',
    });
    $('.select2bs43').select2({
      theme: 'bootstrap4',
    })*/
    /* for for (i = 0; i < 10; i++) 
    {
      $('.select2bs4'.i).select2({
        theme: 'bootstrap4'
      })

    } */
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
</body>
</html>
