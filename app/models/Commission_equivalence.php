<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Commission_equivalence extends Model
{
    protected $table='commission_equivalences';

    public function commision_dossier()
    {
        return $this->belongsToMany(commission_dossier::class,'id_commission_equivalence');
    }
}
