<?php

namespace App\Admin\Controllers;

use App\models\pays;
use App\Models\Universite;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UniversiteController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Universite';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Universite());
         $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->like('nom_fr','nom de l\'universite');
         });
        $grid->column('id', __('Identifiants'));
        $grid->column('nom_fr', __('Nom en français'));
        $grid->column('nom_en', __('Nom en anglais'));
        $grid->column('id_pays', __('Pays'))->display(function ($title,$column){
            $p=pays::find($this->id_pays);
            return $p->nom_fr;

    /*        $u=Universite::all();

            foreach ($p as $pays)
            {
                foreach ($u as $us) {


                    if ($pays->id == $us->id_pays) {
                        $s = $pays->nom_fr;
                        return $s;
                    }
                }

            }*/

        });
        $grid->column('address_mail', __('Emails '));
        $grid->column('created_at', __('Date de création'));
        $grid->column('updated_at', __('Date de mise à jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Universite::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom_fr', __('Nom fr'));
        $show->field('nom_en', __('Nom en'));
        $show->field('id_pays', __('Id pays'));
        $show->field('address_mail', __('Address mail'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Universite());
        $pays=['default'=>'Selectionnez le pays'];
        $p=pays::all();
        foreach ($p as $pay)
        {
            $pays[$pay->id]=$pay->nom_fr;
        }

        $form->text('nom_fr', __('Nom fr'));
        $form->text('nom_en', __('Nom en'));
        $form->select('id_pays', __('Pays'))->options($pays);
        $form->text('address_mail', __('Address mail'));

        return $form;
    }
}
