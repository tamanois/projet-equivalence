@extends('baselogin')

@section('content')
<body class="hold-transition login-page " style="background: url(img/balance.svg) no-repeat center; width:100%;">
<div class="login-box " style="opacity:0.97; " >
  <div class="login-logo" style="margin-bottom:0px; margin-top:50px; box-shadow:1px 1px 5px">
    <div style="height:140px; background:#5B7693; margin:auto;">
        <a href="index2.html" class="" >
          <img src="img/logominesup.jpg" style="height:130px; margin-left:0%; padding:05px;" alt=" Logo" class="brand-image img-circle elevation-3"
               style="opacity: .8">
<!--          <span class="brand-text font-weight-light"></span>-->
        </a>
    </div>
  </div>
  <!-- /.login-logo -->
  <div class="card " style="border-radius:0px; box-shadow:1px 1px 5px">
    <div class="card-body login-card-body ">
      <p class="login-box-msg">Sign in to start your session</p>

      <form method="POST" action="{{ route('login') }}">
                        @csrf
        <div class="input-group mb-3">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email " autofocus>

                                
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope" style="color:#5B7693;"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password" autocomplete="current-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock" style="color:#5B7693;"></span>
            </div>
          </div>
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="row" style="margin-top: 20px;">
          <div class="col-8">
            <div class="icheck-default">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn  btn-block" style="background:#C73F01; color:white;">Sign In
              <i class="fas fa-sign-in-alt"></i> </button>
          </div>
          <!-- /.col -->
        </div>
      </form>


      <p class="mb-1" style="margin-top: 30px;">
        <a href="forgot-password.html" style="color:#5B7693;"> <i class="fas fa-key"></i>  I forgot my password</a>
      </p>
      <p class="mb-0" style="margin-top: 20px;">
        <a href="{{ route('register1') }}" class="text-center" style="color:#5B7693;"> <i class="fas fa-user-plus"></i> Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection