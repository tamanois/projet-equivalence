<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class pays extends Model
{
    protected $table='pays';

    public function dossier()
    {
        return $this->hasMany(Dossier::class,'id_pays');
    }
    public  function universite()
    {
        return $this->hasMany(Universite::class,'id_pays');
    }
    public  function equivalence_diplome_pays()
    {
        return $this->hasMany(equivalence_diplome_pays::class,'id_pays');
    }

}
