<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Universite extends Model
{
    protected $table='universites';
    public  function dossier()
    {
        return $this->hasMany(Dossier::class,'id_universite');
    }
    public function pays()
    {
        return $this->belongsTo(pays::class,'id_pays');
    }

}
