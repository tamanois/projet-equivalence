<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Dossier extends Model
{
    protected $table = 'dossiers';
    protected $guarded = [];
    protected $casts=[
        'diplome'=>'array'
    ];
    public function candidat_utilisateur(){
        return $this->belongsTo(Candidat_utilisateur::class,'id_candidat_utilisateur');
    }
    public function categorie_equivalence()
    {
        return $this->belongsTo(Categorie_equivalence::class,'id_categorie_equivalence');
    }
    public function universite(){
        return $this->belongsTo(Universite::class,'id_universite');
    }
    public function etat()
    {
        return $this->belongsTo(etat::class,'id_etat');
    }
    public function session()
    {
        return $this->belongsTo(Session::class,'id_session');
    }
    public function pays()
    {
        return $this->belongsTo(pays::class,'id_pays');
    }

    public function commision_dossier()
    {
        return $this->belongsToMany(commission_dossier::class,'id_dossier');
    }
    public function coter_dossier()
    {
        return $this->belongsToMany(coter_dossier::class,'id_dossier');
    }
    public function etat_dossier()
{
return $this->belongsToMany(etat_dossier::class,'id_dossier');
}
    public function diplome()
    {
        return $this->belongsTo(diplome::class,'id_diplome');
    }
}
