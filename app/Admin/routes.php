<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('pays', paysController::class);
    $router->resource('parametres', ParametreController::class);
    $router->resource('commission_dossiers', commission_dossierController::class);
    $router->resource('commission_equivalences', Commission_equivalenceController::class);
    $router->resource('candidat_utilisateurs', Candidat_utilisateurController::class);
    $router->resource('categorie_equivalences', Categorie_equivalenceController::class);
    $router->resource('sessions', SessionController::class);
    $router->resource('etats', etatController::class);
    $router->resource('diplomes', diplomeController::class);
    $router->resource('universites', UniversiteController::class);
    $router->resource('role_utilisateurs', Role_utilisateurController::class);
    $router->resource('fonctions', fonctionController::class);
    $router->resource('equivalence_diplome_pays', equivalence_diplome_paysController::class);
    $router->resource('dossiers', DossierController::class);
    $router->resource('etat_dossiers', etat_dossierController::class);
    $router->resource('coter_dossiers', coter_dossierController::class);



});
