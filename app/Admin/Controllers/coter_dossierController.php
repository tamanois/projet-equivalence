<?php

namespace App\Admin\Controllers;

use App\Models\admin_user;
use App\models\coter_dossier;
use App\models\Dossier;
use App\models\etat;
use App\Models\etat_dossier;
use App\models\fonction;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Auth;

class coter_dossierController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'liste des dossiers cotés';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new coter_dossier());

        $grid->column('id', __('Id'));
        $grid->column('id_admin_user_destinaire', __('Id admin user destinaire'));
        $grid->column('id_admin_user_expediteur', __('Id admin user expediteur'));
        $grid->column('id_fonction_expediteur', __('Id fonction expediteur'));
        $grid->column('id_fonction_destinateur', __('Id fonction destinateur'));
        $grid->column('id_etat', __('Id etat'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(coter_dossier::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('id_admin_user_destinaire', __('Id admin user destinaire'));
        $show->field('id_admin_user_expediteur', __('Id admin user expediteur'));
        $show->field('id_fonction_expediteur', __('Id fonction expediteur'));
        $show->field('id_fonction_destinateur', __('Id fonction destinateur'));
        $show->field('id_etat', __('Id etat'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new coter_dossier());
        $d=Admin::user()->id;
        $f=Admin::user()->id_fonction;
        $do=['default'=>'Selectionnez le nom du candidat dont on veut coté le dossier)'];

        $d=Dossier::where('candidat_utilisateur','=','candidat_utilisateurs.id')->get();
        foreach ($d as $des)
        {
            $do[$des->id]=$des->nom;
            /*  dump($do);
              die();*/
        }
        $ad=['default'=>'Selectionnez l\'utilisateur donc vous voulez  coté le dossier)'];
        $ads=admin_user::all();
        foreach ($ads as $df)
        {
            $ad[$df->id]=$df->name;
        }
        $fonct=['default'=>'Selectionnez  la fonction de l\'utilisateur donc vous voulez  coté le dossier)'];

        $fs=fonction::all();
        foreach ($fs as $fg)
        {
            $fonct[$fg->id]=$fg->nom;
        }
        $et=['default'=>'Selectionnez  le statut du dossier)'];
        $etat=etat::all();
        foreach ($etat as $ed)
        {
            $et[$ed->id]=$ed->nom;
        }

        $form->select('id_dossier', __('Nom du candidat concernée'))->options($do);
        $form->select('id_fonction_destinateur', __('Id fonction destinateur'))->options($fonct);
        $form->select('id_admin_user_destinaire', __('Id admin user destinaire'))->options($ad);
        $form->hidden('id_admin_user_expediteur', __('Id admin user expediteur'))->value($d);
        $form->hidden('id_fonction_expediteur', __('Id fonction expediteur'))->value($f);
        $form->select('id_etat', __('Id etat'))->options($et);

        $form->saving(function (Form $form)
        {
           $etat_dossier=new etat_dossier();
           $etat_dossier->id_dossier=$form->id_dossier;
           $etat_dossier->id_etat=$form->id_etat;
           $etat_dossier->save();
        });

        return $form;
    }
}
