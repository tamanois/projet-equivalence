<?php

namespace App\Admin\Controllers;

use App\models\Dossier;
use App\models\etat;
use App\models\etat_dossier;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class etat_dossierController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'etat_dossier';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new etat_dossier());

        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->scope('nom','nom de la fonction')->where('id_dossier','=','dossiers.id')->where('dossiers.candidat_utilisateur','=','candidat_utilisateurs.id');
        });

        $grid->column('id', __('Identifier'));
        $grid->column('id_dossier', __('Nom du candidat'))->display(function ($title,$column)
        {
            $d=Dossier::where('id','=','id_dossier')->where('candidat_utilisateur','=','candidat_utilisateurs.id');
            return ;
           /* dump($d);
            die();*/
        });
        $grid->column('id_etat', __('Satut du dossier'))->display(function ($title,$column)
        {
            $et=etat::find('id_etat');
            return $et->nom;
        });
        $grid->column('created_at', __('Date de creation'));
        $grid->column('updated_at', __('Date de mise a jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(etat_dossier::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('id_dossier', __('Id dossier'));
        $show->field('id_etat', __('Id etat'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new etat_dossier());

        $form->hidden('id_dossier', __('Id dossier'));
        $form->hidden('id_etat', __('Id etat'));

        return $form;
    }
}
