<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class admin_user extends Model
{
    protected $table='admin_users';
    public function fonction()
    {
        return $this->belongsTo(fonction::class,'id_fonction');
    }
    public function coter_dossier_destinataire()
    {
        return $this->belongsToMany(coter_dossier::class,'id_admin_user_destinaire');
    }
    public function coter_dossiers_expediteur()
    {
        return $this->belongsToMany(coter_dossier::class,'id_admin_user_expediteur');
    }
}
