<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class equivalence_diplome_pays extends Model
{
    protected $table='equivalence_diplome_pays';
    protected $casts=[
        'liste_diplome'=>'array'
    ];
    public function pays()
    {
        return $this->belongsTo(pays::class,'id_pays');
    }
    public function diplome()
    {
        return $this->belongsTo(diplome::class,'id_diplome');
    }

}
