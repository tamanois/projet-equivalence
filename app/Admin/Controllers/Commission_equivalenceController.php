<?php

namespace App\Admin\Controllers;

use App\Models\Commission_equivalence;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class Commission_equivalenceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = ' Liste des Commission d\'équivalences';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Commission_equivalence());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->like('nom_fr', 'nom de la commission d\'équivalence');
        });
        $grid->column('id', __('Identifiant'));
        $grid->column('nom_fr', __('Nom français'));
        $grid->column('nom_en', __('Nom anglais'));
        $grid->column('created_at', __('Date de création'));
        $grid->column('updated_at', __('Date de mise à jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Commission_equivalence::findOrFail($id));

        $show->field('id', __('Identifiant'));
        $show->field('nom_fr', __('Nom français'));
        $show->field('nom_en', __('Nom anglais'));
        $show->field('created_at', __('Date de création'));
        $show->field('updated_at', __('Date de mise à jour'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Commission_equivalence());

        $form->text('nom_fr', __('Nom fr'));
        $form->text('nom_en', __('Nom en'));

        return $form;
    }
}
