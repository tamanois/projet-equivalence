<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dossiers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidat_utilisateur')->default(0);
            $table->integer('id_pays')->default(0);
            $table->integer('id_categorie_equivalence')->default(0);
            $table->integer('id_universite')->default(0);
            $table->integer('id_session')->default(0);
            $table->integer('id_etat')->default(0);
            $table->string('diplome_a_authentifie');
            $table->integer('id_diplome')->default(0);
            $table->string('type_diplome')->default(0);
            $table->json('diplome');
            $table->string('acte_naissance');
            $table->string('cni');
            $table->string('annee_obtention_diplome')->default(0);
            $table->date('date_delivrance');
            $table->string('programme_etude');
            $table->date('date_depot')->default(0);
            $table->string('cv');
            $table->string('duree_diplome')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dossiers');
    }
}
