<?php

namespace App\Admin\Controllers;

use App\models\Categorie_equivalence;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class Categorie_equivalenceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Liste des catégories d\'équivalence';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Categorie_equivalence());
        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->like('nom_fr','nom de la catégorie d\'équivalence');
        });

        $grid->column('id', __('Id'));
        $grid->column('nom_fr', __('Nom en français'));
        $grid->column('nom_en', __('Nom en anglais'));
        $grid->column('created_at', __('Date de création'));
        $grid->column('updated_at', __('Date de mise à jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Categorie_equivalence::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('nom_fr', __('Nom fr'));
        $show->field('nom_en', __('Nom en'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Categorie_equivalence());

        $form->text('nom_fr', __('Nom en français'));
        $form->text('nom_en', __('Nom en anglais'));

        return $form;
    }
}
