<?php

namespace App\Admin\Controllers;

use App\Models\admin_user;
use App\models\Categorie_equivalence;
use App\Models\commission_dossier;
use App\models\Commission_equivalence;
use App\models\Dossier;
use App\models\Session;
use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use GuzzleHttp\Psr7\Request;

class commission_dossierController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Liste des dossiers classés par commision/sous commission';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new commission_dossier());

        $grid->column('id', __('Identifiants'));
        $grid->column('id_dossier', __('Nom du titulaire du  dossier'))->display(function ($title,$column)
        {
            $c=Dossier::find($this->id_dossier)->where('candidat_utilisateur','=','candidat_utilisateurs.id')->first();
            return $c->nom.' '.$c->prenom;
        });
        $grid->column('id_commission_equivalence', __('commission d\'equivalence'));
        $grid->column('id_session', __('session'));
        $grid->column('id_user', __('utilisateur'));
        $grid->column('menbres', __('Menbres'));
        $grid->column('chef_commission', __('Chef de la  commission'));
        $grid->column('created_at', __('Date de creation'));
        $grid->column('updated_at', __('Date de mise a jour'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(commission_dossier::findOrFail($id));

        $show->field('id', __('Identifiants'));
        $show->field('id_dossier', __('Nom du titulaire du  dossier'));
        $show->field('id_commission_equivalence', __('commission d\'equivalence'));
        $show->field('id_session', __('session'));
        $show->field('id_user', __('Utilisateur'));
        $show->field('menbres', __('Menbres'));
        $show->field('chef_commission', __('Chef de la commission'));
        $show->field('created_at', __('Date de creation'));
        $show->field('updated_at', __('Date de mise a jour'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new commission_dossier());
        $admin_u=['default'=>'Selectionnez l\'utilisateur qui cote le dossier(il s\'agit de celui qui est connecté actuellement)'];
        $ad=admin_user::all();
        foreach ($ad as $admin)
        {
            $admin_u[$admin->id]=$admin->name;
        }
        $do=['default'=>'Selectionnez le nom du candidat dont on veut classer le dossier)'];

        $d=Dossier::where('candidat_utilisateur','=','candidat_utilisateurs.id')->get();
        foreach ($d as $des)
        {
            $do[$des->id]=$des->nom;
          /*  dump($do);
            die();*/
        }
        $co=['default'=>'Selectionnez la commission nationale habilité à traiter le dossier'];
        $commision=Commission_equivalence::all();
        foreach ($commision as $ca)
        {
            $co[$ca->id]=$ca->nom_fr;

        }
        $s=['default'=>'Selectionnez la session d\'équivalence concernée'];
        $se=Session::all();
        foreach ($se as $session)
        {
            $s[$session->id]=$session->nom;

        }
        $d=Admin::user()->id;



        $form->select('id_dossier', __('Nom du candidats concerné par le dossier'))->options($do);
        $form->select('id_commission_equivalence', __('commission d\'equivalence'))->options($co);
        $form->select('id_session', __('session'))->options($s);
        $form->hidden('id_user', __('Utilisateur qui traite le dossier'))->value($d);
        $form->textarea('menbres', __('Nom des menbres de la commission'));
        $form->text('chef_commission', __('Chef  de la commission'));


        return $form;
    }
}
