<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class commission_dossier extends Model
{
    protected $table='commission_dossiers';
    protected $casts=[
        'menbres'=>'array'
    ];

    public function commission_equivalence()
    {
        return $this->belongsToMany(Commission_equivalence::class,'id_commission_equivalence');
    }
    public function dosiier()
    {
        return $this->belongsToMany(Dossier::class,'id_dossier');
    }
    public function session()
    {
        return $this->belongsToMany(Session::class,'id_session');
    }
    public function user()
    {
        return $this->belongsToMany(User::class,'id_user');
    }

}
