<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Candidat_utilisateur;

class CandidatController extends Controller
{
    //
    protected $values=[];
    public function __construct()
    {
       
        $this->values['big_title']='ACCUEIL';

        $this->values['title']='accueil';
        
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        //
        $user = $request->user();

        
        $this->values['user']=$user;
       
        return view('index', $this->values);

    }
}
