<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class diplome extends Model
{
    protected $table='diplomes';

    protected $casts=[
        'diplome'=>'array'
    ];
    public function equivalence_diplome_pays()
    {
        return $this->hasMany(equivalence_diplome_pays::class,'id_diplome');
    }
    public function dossier()
    {
        return $this->belongsTo(Dossier::class,'id_diplome');
    }

}
